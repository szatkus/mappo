import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core'

@Component({
  selector: 'mappo-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent {
  readonly ANIMATION_DURATION = 1000
  isVisible = false
  isClicked = false
  @Output() done: EventEmitter<number> = new EventEmitter<number>()
  @ViewChild('button') button!: ElementRef<HTMLButtonElement>

  onClick (): void {
    this.isClicked = true
    this.button.nativeElement.addEventListener('transitionend', () => {
      this.isVisible = false
      this.done.emit(0)
    }, { once: true })
  }

  public appear (): void {
    this.isVisible = true
    this.isClicked = false
  }

  public async waitForClick (): Promise<number> {
    return await new Promise((resolve) => {
      this.done.subscribe(resolve)
    })
  }
}
