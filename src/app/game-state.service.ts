import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class GameStateService {
  private gameState = {
    prologDone: false,
    stamina: 100,
    maxStamina: 100,
    staminaExp: 0,
    knowledgePoints: 0,
    energy: 0
  }

  constructor () {
    this.load()
  }

  public finishProlog (): void {
    this.gameState.prologDone = true
    this.save()
  }

  public isPrologDone (): boolean {
    return this.gameState.prologDone
  }

  public addStaminaExp (value = 1): void {
    this.gameState.staminaExp = (this.gameState.staminaExp ?? 0) + value
    this.save()
  }

  public getStaminaExp (): number {
    return this.gameState.staminaExp ?? 0
  }

  public reduceStamina (value = 1): void {
    this.gameState.stamina = (this.gameState.stamina ?? 0) - value
    this.save()
  }

  public getStamina (): number {
    return this.gameState.stamina ?? 0
  }

  public getMaxStamina (): number {
    return this.gameState.maxStamina ?? 0
  }

  public addKnowledgePoints (value = 1): void {
    this.gameState.knowledgePoints = (this.gameState.knowledgePoints ?? 0) + value
    this.save()
  }

  public getKnowledgePoints (): number {
    return this.gameState.knowledgePoints ?? 0
  }

  public addEnergy (value = 1): void {
    this.gameState.energy = (this.gameState.energy ?? 0) + value
    this.save()
  }

  public getEnergy (): number {
    return this.gameState.energy ?? 0
  }

  private load (): void {
    const gameState = localStorage.getItem('gameState')
    if (gameState !== null) {
      this.gameState = JSON.parse(gameState)
    }
  }

  private save (): void {
    localStorage.setItem('gameState', JSON.stringify(this.gameState))
  }
}
