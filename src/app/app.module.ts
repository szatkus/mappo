import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './app.component'
import { SpeechComponent } from './speech/speech.component'
import { ButtonComponent } from './button/button.component';
import { StatusbarComponent } from './statusbar/statusbar.component'

@NgModule({
  declarations: [
    AppComponent,
    SpeechComponent,
    ButtonComponent,
    StatusbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
