import { Component, Input } from '@angular/core'

@Component({
  selector: 'mappo-statusbar',
  templateUrl: './statusbar.component.html',
  styleUrls: ['./statusbar.component.css']
})
export class StatusbarComponent {
  @Input() knowledgePoints!: number
  @Input() stamina!: number
  @Input() maxStamina!: number
  @Input() energy!: number
}
