import { Component, ElementRef, ViewChild } from '@angular/core'
import { SpeechQueueService } from '../speech-queue.service'

@Component({
  selector: 'mappo-speech',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.css']
})
export class SpeechComponent {
  private readonly DURATION_PER_CHARACTER = 50
  private readonly ANIMATION_DURATION = 1000
  private isConsuming = false
  @ViewChild('bubble') bubble!: ElementRef<HTMLDivElement>
  currentLine = ''
  opacity = 0

  constructor (private readonly speechQueue: SpeechQueueService) {
    this.speechQueue.poll().subscribe(() => { void this.consume() })
  }

  private async waitUntilTransition (): Promise<unknown> {
    return await new Promise(resolve => {
      this.bubble.nativeElement.addEventListener('transitionend', resolve, { once: true })
    })
  }

  private async consume (): Promise<unknown> {
    if (this.isConsuming) {
      return
    }
    this.isConsuming = true
    let line = this.speechQueue.pick()
    while (line !== undefined) {
      this.currentLine = line.text
      this.opacity = 1
      await this.waitUntilTransition()
      this.opacity = 0
      await this.waitUntilTransition()
      line.done()
      line = this.speechQueue.pick()
    }
    this.currentLine = ''
    this.isConsuming = false
    return null
  }
}
