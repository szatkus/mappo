import { Injectable } from '@angular/core'
import { Observable, type Subscriber } from 'rxjs'

export interface TextLine {
  text: string
  done: () => void
}

@Injectable({
  providedIn: 'root'
})
export class SpeechQueueService {
  private readonly queue: TextLine[] = []
  private subscriber: Subscriber<undefined> | undefined

  public async addLine (text: string): Promise<unknown> {
    return await new Promise(resolve => {
      const chunk = {
        text,
        done: () => { resolve(null) }
      }
      this.queue.push(chunk)
      this.subscriber?.next()
    })
  }

  public poll (): Observable<undefined> {
    return new Observable((subscriber) => {
      this.subscriber = subscriber
      if (this.queue.length > 0) {
        subscriber.next()
      }
    })
  }

  public pick (): TextLine | undefined {
    return this.queue.shift()
  }
}
