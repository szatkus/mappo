import { Component, ViewChild } from '@angular/core'
import { SpeechQueueService } from './speech-queue.service'
import { ButtonComponent } from './button/button.component'
import { GameStateService } from './game-state.service'

async function delay (ms: number): Promise<unknown> {
  return await new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

@Component({
  selector: 'mappo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mappo'

  @ViewChild('door') door!: ButtonComponent
  @ViewChild('rest') rest!: ButtonComponent
  @ViewChild('ladder') ladder!: ButtonComponent

  constructor (private readonly queue: SpeechQueueService, public gameState: GameStateService) {
    if (!gameState.isPrologDone()) {
      setTimeout(async () => {
        await queue.addLine('Finally...')
        await queue.addLine('After all those months...')
        void queue.addLine('The end of the tunnel...')
        await delay(800)
        this.door.appear()
        await this.door.waitForClick()
        await queue.addLine('There\'s a room...')
        await queue.addLine('Another door...')
        this.door.appear()
        await this.door.waitForClick()
        await queue.addLine('It\'s some kind of a bedroom...')
        await queue.addLine('At least it seems like a place where I could get some rest.')
        await queue.addLine('The sheet is in a bad shape, but after I clean the mattress, it should be fine.')
        await queue.addLine('There\'s also a ladder that leads to the hatch in the ceiling.')
        await queue.addLine('But I\'ll go to sleep first.')
        this.gameState.finishProlog()
      }, 1)
    }
  }

  clickDoor (): void {
    this.gameState.addStaminaExp()
    this.gameState.reduceStamina()
  }

  openDoor (): void {
    this.gameState.addKnowledgePoints()
  }
}
